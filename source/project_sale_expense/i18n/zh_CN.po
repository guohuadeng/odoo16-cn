# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* project_sale_expense
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~15.4\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-08-30 10:57+0000\n"
"PO-Revision-Date: 2022-08-30 18:58+0800\n"
"Last-Translator: odooAi.cn <support@odooai.cn>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: \n"
"Language: zh_CN\n"
"X-Generator: Poedit 2.4.1\n"

#. module: project_sale_expense
#: model:ir.model,name:project_sale_expense.model_project_project
msgid "Project"
msgstr "项目"
